﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JsonLoader : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        string loadMyJson = JsonReader.LoadJson("Commands.json");

        if (Input.GetKeyDown(KeyCode.F))
        {
            jsonText.GetComponent<Text>().text = loadMyJson;
            textObject.SetActive(true);
        }
        if(Input.GetKeyUp(KeyCode.F))
        {
           textObject.SetActive(false);
        }
    }
}
